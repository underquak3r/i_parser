<?php

namespace Drupal\i_parser;

use DOMXPath;
use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class BanksParser {

  const site_domain = 'DOMAIN';

  const site_scheme = 'https://';

  const site_url = self::site_scheme . self::site_domain;

  const city_vid = 'cities';

  const currency_vid = 'currency';

  const bank_type = 'bank';

  const course_type = 'course';
  const course_cbrf_type = 'course_cbrf';

  const banks_url = self::site_url . '/bank';

  const guzzle_client_options = [
    'headers' => [
      'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0',
    ],
    'curl' => [
      CURLOPT_INTERFACE => 'eth0:1',
    ],
    'verify' => FALSE,
  ];

  const email_to_notice = [
    'prozorovks@gmail.com',
  ];

  public function __construct() {
    set_time_limit(0);
  }

  protected function log($data, $label = NULL, $with_time = TRUE) {
    $out = '';
    if ($with_time) {
      $out = date('H:i:s d-m-Y', time()) . "\n";
    }
    $out .= ($label ? $label . ': ' : '') . print_r($data, TRUE) . "\n";
    $file = '/var/www/i_parser/data/mod-tmp/bank_debug.txt';
    file_put_contents($file, $out, FILE_APPEND);
  }

  /**
   * Отправка писем
   *
   * @param $subject
   * @param $message
   * @param $to
   *
   * @return mixed
   */
  protected function sendMail($subject, $message, $to) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'i_parser';
    $key = 'parser_notice';
    $params['subject'] = $subject;
    $params['message'] = $message;
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;

    return $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
  }

  /**
   * Отправка писем всем
   *
   * @param $subject
   * @param $message
   */
  protected function sendMailToAll($subject, $message) {
    foreach (self::email_to_notice as $email) {
      $this->sendMail($subject, $message, $email);
    }
  }

  /**
   * Создание термина города если такого еще нет
   *
   * @param $name
   * @param $code
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createCity($name, $code) {
    if ($name && $code) {
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('field_code', $code);
      $query->condition('vid', self::city_vid);
      $tids = $query->execute();

      if (!$tids) {
        Term::create([
          'name' => $name,
          'vid' => self::city_vid,
          'field_code' => $code,
        ])->save();
      }
    }
  }

  protected function createCurrency($name, $code) {
    if ($name && $code) {
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('field_code', $code);
      $query->condition('vid', self::currency_vid);
      $tids = $query->execute();

      if (!$tids) {
        Term::create([
          'name' => $name,
          'vid' => self::currency_vid,
          'field_code' => $code,
        ])->save();
      }
    }
  }

  protected function createBank($name, $code) {
    if ($name && $code) {
      $query = \Drupal::entityQuery('node');
      $query->condition('field_code', $code);
      $query->condition('type', 'bank');
      $nids = $query->execute();

      if (!$nids) {
        Node::create([
          'type' => 'bank',
          'title' => $name,
          'field_code' => $code,
        ])->save();
      }
    }
  }

  /**
   * Получение городов
   *
   * @return false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseCitiesFromHead() {
    $client = \Drupal::httpClient();
    $request = $client->get(self::site_url, self::guzzle_client_options);
    $status = $request->getStatusCode();
    if ($status != 200) {
      return FALSE;
    }

    $file_contents = $request->getBody()->getContents();

    $html = Html::load($file_contents);

    $xpath = new DomXPath($html);
    $classname = 'xxx-modal-search-city__city-list';
    $uls = $xpath->query("//ul[contains(@class, '$classname')]");


    if (!is_null($uls)) {
      foreach ($uls as $ul) {
        foreach ($ul->childNodes as $li) {
          /* @var \DOMElement $li */
          $link = stripslashes($li->firstChild->getAttribute('data-link'));
          $link = str_replace('"', "", $link);
          $link = str_replace("'", "", $link);
          $link = parse_url($link, PHP_URL_HOST);
          $host_parts = explode('.', $link);
          $this->createCity(trim($li->firstChild->textContent), $host_parts[0]);
        }
      }
    }
  }

  /**
   * Получение кодов городов из XML файла
   *
   * @return array
   */
  protected function getCitiesCodesFromXml() {
    $city_codes = [];
    $xml = file_get_contents('/var/www/i_parser/data/www/i_parser/web/sites/default/files/xml/sitemap-currency.xml');
    $xml_object = new \SimpleXMLElement($xml);
    foreach ($xml_object->url as $item) {
      $url = (string) $item->loc;
      $path = explode('/', ltrim(parse_url($url)['path'], '/'));
      if (count($path) == 3) {
        $city_codes[$path[2]] = $path[2];
      }
    }

    return $city_codes;
  }

  /**
   * Получение кодов городов из XML, название получаем с сайта
   *
   * @return false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseCitiesFromXml() {
    ini_set('max_execution_time', 3600);
    $city_codes = $this->getCitiesCodesFromXml();
    foreach ($city_codes as $city_code) {

      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('field_code', $city_code);
      $query->condition('vid', self::city_vid);
      $tids = $query->execute();

      if (!$tids) {
        $client = \Drupal::httpClient();
        $url = 'https://DOMAIN/currency/' . $city_code;
        $request = $client->get($url, self::guzzle_client_options);
        $status = $request->getStatusCode();
        if ($status != 200) {
          return FALSE;
        }

        $file_contents = $request->getBody()->getContents();
        $html = Html::load($file_contents);

        $xpath = new DomXPath($html);
        $classname = 'settings-nav__city';
        $spans = $xpath->query("//span[contains(@class, '$classname')]");

        if (!empty($spans)) {
          $this->createCity($spans->item(0)->textContent, $city_code);
        }
      }
    }
  }

  /**
   * Получение банков
   *
   * @return false
   */
  public function parseBanks() {
    // начинаем с первой страницы
    $current_page = 1;
    $have_next_page = TRUE;
    // если есть следующая страница
    while ($have_next_page) {
      $have_next_page = FALSE;
      $client = \Drupal::httpClient();
      $url = self::banks_url;
      // для первой страницы параметр page не указываем
      if ($current_page > 1) {
        $url .= '?page=' . $current_page;
      }

      $request = $client->get($url, self::guzzle_client_options);
      $status = $request->getStatusCode();
      if ($status != 200) {
        return FALSE;
      }

      $file_contents = $request->getBody()->getContents();
      $html = Html::load($file_contents);

      // получаем следующую страницу
      $xpath = new DomXPath($html);
      $classname = 'pagination';
      $ul = $xpath->query("//ul[contains(@class, '$classname')]");
      if (!empty($ul)) {
        foreach ($ul->item(0)->childNodes as $li) {
          if ($li->firstChild->tagName == 'a') {
            $page_num = $li->firstChild->getAttribute('data-page');
            if (($page_num + 1) > $current_page) {
              $current_page = $page_num + 1;
              $have_next_page = TRUE;
              break;
            }
          }
        }
      }

      // обходим таблицу с банками
      $xpath = new DomXPath($html);
      $id = 'tablecurr';
      $table = $xpath->query("//table[contains(@id, '$id')]");
      if (!empty($table)) {
        foreach ($table->item(0)->lastChild->childNodes as $tr) {
          /* @var \DOMElement $a */
          $a = $tr->childNodes->item(1)->firstChild->firstChild;
          $bank_name = trim($a->textContent);
          $href = explode('/', ltrim($a->getAttribute('href'), '/'));

          $this->createBank($bank_name, $href[1]);
        }
      }
    }
  }

  /**
   * Получение списка банков в городе на странице курсов
   *
   * @param $city - код города
   *
   * @return array
   */
  protected function parseBanksInCity($city) {
    $bank_ids = [];
    // начинаем с первой страницы
    $current_page = 1;
    $have_next_page = TRUE;
    // если есть следующая страница
    while ($have_next_page) {
      $have_next_page = FALSE;
      $client = \Drupal::httpClient();
      $url = self::site_scheme . $city . '.' . self::site_domain . '/currency';
      // для первой страницы параметр page не указываем
      if ($current_page > 1) {
        $url .= '?page=' . $current_page;
      }

      $request = $client->get($url, self::guzzle_client_options);
      $status = $request->getStatusCode();
      if ($status != 200) {
        return FALSE;
      }

      $file_contents = $request->getBody()->getContents();
      $html = Html::load($file_contents);

      // получаем следующую страницу
      $xpath = new DomXPath($html);
      $classname = 'pagination';
      $ul = $xpath->query("//ul[contains(@class, '$classname')]");
      if ($ul->length) {
        foreach ($ul->item(0)->childNodes as $li) {
          if ($li->firstChild->tagName == 'a') {
            $page_num = $li->firstChild->getAttribute('data-page');
            if (($page_num + 1) > $current_page) {
              $current_page = $page_num + 1;
              $have_next_page = TRUE;
              break;
            }
          }
        }
      }

      // обходим таблицу с банками
      $xpath = new DomXPath($html);
      $classname = 'currTable';
      $hrefs = $xpath->query("//table[contains(@class, '$classname')]//tr[contains(@class, 'body')]//a");
      if ($hrefs->length) {
        foreach ($hrefs as $href) {
          /* @var \DOMElement $href */
          $bank_name = trim($href->textContent);
          $href = explode('/', ltrim($href->getAttribute('href'), '/'));
          $bank_id = $this->getBankIdByCode($href[1]);
          if ($bank_id) {
            $bank_ids[] = $bank_id;
          }
          else {
            $this->createBank($bank_name, $href[1]);
          }
        }
      }
    }

    return $bank_ids;
  }

  /**
   * Обновление списка банков у термина города
   *
   * @param $tid
   * @param $banks
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updateBanksInCityTerm($tid, $banks) {
    $term = Term::load($tid);
    $banks_target_id = [];

    foreach ($banks as $bank) {
      $banks_target_id[] = ['target_id' => $bank];
    }

    $term->field_bank_link = $banks_target_id;
    $term->save();
  }

  public function updateBanksInCities() {
    $cities = $this->getCities();
    foreach ($cities as $city) {
      $this->log($city['code'], '$city');
      $banks = $this->parseBanksInCity($city['code']);
      $this->updateBanksInCityTerm($city['tid'], $banks);
    }
  }

  protected function updateBankDetails($bank) {
    $client = \Drupal::httpClient();

    $url = self::site_scheme . self::site_domain . '/bank/' . $bank['code'];
    $request = $client->get($url, self::guzzle_client_options);
    $status = $request->getStatusCode();
    if ($status != 200) {
      return FALSE;
    }

    $bank_fields = [];

    $file_contents = $request->getBody()->getContents();

    $html = Html::load($file_contents);

    $xpath = new DomXPath($html);

    // короткое название
    $name_short = $xpath->query("//h1");
    if ($name_short->length) {
      $bank_fields['field_short_name'] = trim($name_short->item(0)->textContent);
    }

    // полное наименование
    $name_full = $xpath->query("//p[contains(@class, 'society')]");
    if ($name_full->length) {
      $bank_fields['field_full_name'] = trim($name_full->item(0)->childNodes->item(0)->textContent);
    }

    // ссылка
    $link_attributes = $xpath->query("//p[contains(@class, 'society')]");
    if ($link_attributes->length) {
      foreach ($link_attributes->item(0)->childNodes->item(1)->attributes as $attribute) {
        if ($attribute->name == 'href') {
          $bank_fields['field_bank_link'] = trim($attribute->value);
        }
      }
    }

    // адрес и телефоны
    $bank_info = $xpath->query("//div[contains(@class, 'bank-info')]");
    if ($bank_info->length) {
      $bank_fields['field_head_office_address'] = trim($bank_info->item(0)->childNodes->item(0)->childNodes->item(2)->textContent);

      if ($bank_info->item(0)->childNodes->item(1)->childNodes->length) {
        if ($bank_info->item(0)->childNodes->item(1)->childNodes->item(2)->childNodes->length) {
          foreach ($bank_info->item(0)->childNodes->item(1)->childNodes->item(2)->childNodes as $childNode) {
            $bank_fields['field_phone'][] = trim($childNode->textContent);
          }
        }
      }
    }

    // активы депозиты кредиты уставной капитал
    $assets = $xpath->query("//div[contains(@class, 'assets')]");
    if ($assets->length) {
      if ($assets->item(0)->childNodes->length == 4) {
        $bank_fields['field_net_assets'] = trim($assets->item(0)->childNodes->item(0)->childNodes->item(1)->textContent);
        $bank_fields['field_deposits'] = trim($assets->item(0)->childNodes->item(1)->childNodes->item(1)->textContent);
        $bank_fields['field_credits'] = trim($assets->item(0)->childNodes->item(2)->childNodes->item(1)->textContent);
        $bank_fields['field_authorized_capital'] = trim($assets->item(0)->childNodes->item(3)->childNodes->item(1)->textContent);
      }
    }

    // справочная информация
    $info = $xpath->query("//div[contains(@class, 'aboutBank')]//div[contains(@class, 'info')]");
    if ($info->length) {
      if ($info->item(0)->childNodes->length) {
        foreach ($info->item(0)->childNodes as $childNode) {
          /* @var \DOMElement $childNode */
          if ($childNode->tagName == 'p') {
            if ($childNode->childNodes->length) {
              if ($childNode->childNodes->item(0)->tagName == 'span' && trim($childNode->childNodes->item(0)->textContent) == 'Регистрационный номер:') {
                $bank_fields['field_registration_number'] = trim($childNode->childNodes->item(1)->textContent);
              }

              if ($childNode->childNodes->item(0)->tagName == 'span' && trim($childNode->childNodes->item(0)->textContent) == 'Дата регистрации Банком России:') {
                $bank_fields['field_date_of_registration'] = trim($childNode->childNodes->item(1)->textContent);
              }

              if ($childNode->childNodes->item(0)->tagName == 'span' && trim($childNode->childNodes->item(0)->textContent) == 'БИК:') {
                $bank_fields['field_bik'] = trim($childNode->childNodes->item(1)->textContent);
              }
            }
          }
        }
      }
    }

    $node = Node::load($bank['nid']);
    foreach ($bank_fields as $field_name => $value) {
      $node->set($field_name, $value);
    }
    $node->save();

    $this->log($bank['nid'], 'updateBankDetails');
  }

  public function updateBanksDetails() {
    $banks = $this->getBanks();
    foreach ($banks as $bank) {
      $this->log($bank);
      $this->updateBankDetails($bank);
    }
  }

  protected function updateBankImages($bank) {
    $node = Node::load($bank['nid']);
    $logo = file_get_contents('https://api.' . self::site_domain . '/bank_logo/logos/' . $bank['code'] . '.png');
    if ($logo) {
      $file = file_save_data($logo, 'public://bank/logo/' . $bank['code'] . '.png', FileSystemInterface::EXISTS_REPLACE);
      $node->set('field_logo', $file->id());
    }

    $icon = file_get_contents('https://api.' . self::site_domain . '/bank_logo/icons/' . $bank['code'] . '.png');
    if ($icon) {
      $file = file_save_data($icon, 'public://bank/icon/' . $bank['code'] . '.png', FileSystemInterface::EXISTS_REPLACE);
      $node->set('field_icon', $file->id());
    }
    $node->save();
  }

  public function updateBanksImages() {
    $banks = $this->getBanks();
    foreach ($banks as $bank) {
      $this->log($bank, 'updateBanksImages');
      $this->updateBankImages($bank);
    }
  }

  /**
   * Получение городов
   *
   * @return array
   */
  public function getCities() {
    $cities = [];

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', self::city_vid);
    $tids = $query->execute();

    foreach ($tids as $tid) {
      $term = Term::load($tid);
      $cities[] = [
        'tid' => $tid,
        'name' => $term->get('name')->value,
        'code' => $term->get('field_code')->value,
      ];
    }

    return $cities;
  }

  /**
   * Получение банков
   *
   * @return array
   */
  public function getBanks() {
    $banks = [];

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'bank');
    $nids = $query->execute();

    foreach ($nids as $nid) {
      $term = Node::load($nid);
      $banks[] = [
        'nid' => $nid,
        'name' => $term->get('title')->value,
        'code' => $term->get('field_code')->value,
      ];
    }

    return $banks;
  }

  /**
   * Получение id валюты по коду
   *
   * @param $code
   *
   * @return false|mixed
   */
  protected function getCurrencyIdByCode($code) {
    $result = FALSE;
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', self::currency_vid);
    $query->condition('field_code', $code);
    $tids = $query->execute();

    if ($tids) {
      $result = current($tids);
    }

    return $result;
  }

  /**
   * Получение города по коду
   *
   * @param $code
   *
   * @return false|mixed
   */
  public function getCityIdByCode($code) {
    $result = FALSE;
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', self::city_vid);
    $query->condition('field_code', $code);
    $tids = $query->execute();

    if ($tids) {
      $result = current($tids);
    }

    return $result;
  }

  /**
   * Получение банка по коду
   *
   * @param $code
   *
   * @return false|mixed
   */
  public function getBankIdByCode($code) {
    $result = FALSE;
    $query = \Drupal::entityQuery('node');
    $query->condition('type', self::bank_type);
    $query->condition('field_code', $code);
    $nids = $query->execute();

    if ($nids) {
      $result = current($nids);
    }

    return $result;
  }

  /**
   * @param $city
   * @param $bank
   * @param $currency
   * @param $buy
   * @param $sell
   * @param $unit
   * @param string $updated - в формате 05.03.2021 19:15
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createCurrencyRate($city, $bank, $currency, $buy, $sell, $unit, $updated) {
    $city_id = $this->getCityIdByCode($city);
    $bank_id = $this->getBankIdByCode($bank);
    $currency_id = $this->getCurrencyIdByCode($currency);

    if ($city_id && $bank_id && $currency_id) {
      $updated_date = new \DateTime();
      $updated_date->setTimestamp(strtotime($updated));
      $query = \Drupal::entityQuery('node');
      $query->condition('type', self::course_type);
      $query->condition('field_date_updated', $updated_date->format('d.m.Y'));
      $query->condition('field_bank', $bank_id);
      $query->condition('field_currency', $currency_id);
      $query->condition('field_city', $city_id);
      $nids = $query->execute();

      // если такой курс есть - обновляем его
      if ($nids) {
        $node = Node::load(current($nids));
        $node->set('field_unit', $unit);
        $node->set('field_updated', $updated);
        $node->set('field_purchase', $buy);
        $node->set('field_sale', $sell);
        $node->save();
      }
      else {
        Node::create([
          'type' => 'course',
          'title' => $city . ' ' . $bank . ' ' . $currency . ' ' . $updated_date->format('d.m.Y'),
          'field_city' => [
            'target_id' => $city_id,
          ],
          'field_bank' => [
            'target_id' => $bank_id,
          ],
          'field_currency' => [
            'target_id' => $currency_id,
          ],
          'field_unit' => $unit,
          'field_updated' => $updated,
          'field_date_updated' => $updated_date->format('d.m.Y'),
          'field_purchase' => $buy,
          'field_sale' => $sell,
        ])->save();
      }
    }
  }

  /**
   * Получение курса по городу и банку
   *
   * @param $city
   * @param $bank
   * @param null $date
   *
   * @return false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function parseCurrencyForCityBank($city, $bank, $date = NULL) {
    $client = \Drupal::httpClient();

    $url = self::site_scheme . $city . '.' . self::site_domain . '/bank/' . $bank . '/currency';
    if ($date) {
      $url .= '/date/' . $date;
    }
    $request = $client->get($url, self::guzzle_client_options);
    $status = $request->getStatusCode();
    if ($status != 200) {
      return FALSE;
    }

    $file_contents = $request->getBody()->getContents();

    $html = Html::load($file_contents);

    $xpath = new DomXPath($html);
    $classname = 'new_curr';
    $table = $xpath->query("//table[contains(@class, '$classname')]");

    if ($table->length) {
      $rows = $table->item(0)->childNodes->item(1)->childNodes;
      foreach ($rows as $tr) {
        /* @var \DOMElement $currency_td */
        /* @var \DOMElement $tr */
        $currency_td = $tr->childNodes->item(0)->firstChild;

        $unit = $currency_td->getAttribute('data-currency');
        if (!$unit) {
          $unit = 1;
        }

        $currency_code = explode('/', ltrim(parse_url($currency_td->getAttribute('href'))['path'], '/'))[3];
        $currency_name = trim($currency_td->textContent);

        $this->createCurrency($currency_name, $currency_code);

        $buy = $tr->childNodes->item(1)->textContent;
        $sell = $tr->childNodes->item(2)->textContent;
        $updated = $tr->childNodes->item(4)->textContent;

        $this->createCurrencyRate($city, $bank, $currency_code, $buy, $sell, $unit, $updated);
      }
    }
  }

  /**
   * Получение привязанных банков к термину города
   *
   * @param $tid
   *
   * @return array
   */
  public function getBanksInCity($tid) {
    $banks_array = [];
    $term = Term::load($tid);
    $banks = $term->get('field_bank_link');
    foreach ($banks->referencedEntities() as $bank) {
      $banks_array[] = [
        'code' => $bank->get('field_code')->value,
      ];
    }

    return $banks_array;
  }

  /**
   * Получение курсов валют
   *
   * @param null $date
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseCurrency($date = NULL) {
    $start_time = time();
    $cities = $this->getSliceCitiesPool();

    $current_city = 0;
    $count_city = count($cities);

    foreach ($cities as $city) {
      $city_tid = $this->getCityIdByCode($city['code']);
      $banks = $this->getBanksInCity($city_tid);
      $count_bank = count($banks);
      $current_city++;
      $this->log("$current_city of $count_city banks_count = $count_bank", 'city', FALSE);
      foreach ($banks as $bank) {
        $this->parseCurrencyForCityBank($city['code'], $bank['code'], $date);
      }
    }
    if ($count_city) {
      $this->log(time() - $start_time . ' sec.', 'time diff for ' . $count_city . ' cities');
    }
  }

  /**
   * Создание / обновление отделения банка
   *
   * @param $city
   * @param $bank
   * @param array $department - массив значений отделения
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createUpdateDepartment($city, $bank, $department) {
    $new_nid = FALSE;
    $city_id = $this->getCityIdByCode($city);
    $bank_id = $this->getBankIdByCode($bank);

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'department');
    $query->condition('field_bank', $bank_id);
    $query->condition('field_city', $city_id);
    $query->condition('title', $department['name']);
    $nids = $query->execute();

    // если такое отделение есть - обновляем его
    if ($nids) {
      $node = Node::load(current($nids));
      $node->set('field_address', $department['address']);
      $node->set('field_metro', $department['metro']);
      $node->set('field_phone', $department['phone']);
      $node->set('field_work_time', $department['work_time']);
      $node->save();
    }
    else {
      Node::create([
        'type' => 'department',
        'title' => $department['name'],
        'field_city' => [
          'target_id' => $city_id,
        ],
        'field_bank' => [
          'target_id' => $bank_id,
        ],
        'field_address' => $department['address'],
        'field_metro' => $department['metro'],
        'field_phone' => $department['phone'],
        'field_work_time' => $department['work_time'],
      ])->save();
      $new_nid = TRUE;
    }

    return $new_nid;
  }

  /**
   * Получение отделений в указанном городе для указанного банка
   *
   * @param $city
   * @param $bank
   *
   * @return false|array
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function parseDepartmentsByCityByBank($city, $bank) {
    $new_department_addresses = FALSE;
    // начинаем с первой страницы
    $current_page = 1;
    $have_next_page = TRUE;
    // если есть следующая страница
    while ($have_next_page) {
      $have_next_page = FALSE;
      $client = \Drupal::httpClient();

      $url = self::site_scheme . $city . '.' . self::site_domain . '/bank/' . $bank . '/departments-tab';
      // для первой страницы параметр page не указываем
      if ($current_page > 1) {
        $url .= '?page=' . $current_page;
      }

      $request = $client->get($url, self::guzzle_client_options);
      $status = $request->getStatusCode();
      if ($status != 200) {
        return FALSE;
      }

      $file_contents = $request->getBody()->getContents();
      $html = Html::load($file_contents);

      // получаем следующую страницу
      $xpath = new DomXPath($html);
      $classname = 'pagination';
      $ul = $xpath->query("//ul[contains(@class, '$classname')]");
      if ($ul->length) {
        foreach ($ul->item(0)->childNodes as $li) {
          if ($li->firstChild->tagName == 'a') {
            $page_num = $li->firstChild->getAttribute('data-page');
            if (($page_num + 1) > $current_page) {
              $current_page = $page_num + 1;
              $have_next_page = TRUE;
              break;
            }
          }
        }
      }

      // обходим таблицу с банками
      $xpath = new DomXPath($html);
      $divs = $xpath->query("//table[contains(@class, 'xxx-table-list')]//tbody//div[@class='xxx-table-branches-bank__item-inner']");

      if ($divs->length) {
        foreach ($divs as $div) {
          $department = [
            'name' => NULL,
            'address' => NULL,
            'metro' => NULL,
            'phone' => NULL,
            'work_time' => NULL,
          ];
          /* @var \DOMElement $div */
          foreach ($div->childNodes as $childNode) {
            /* @var \DOMElement $childNode */
            $class = $childNode->getAttribute('class');


            if ($class == 'xxx-table-branches-bank__item-name-address') {
              foreach ($childNode->childNodes as $address_items) {
                /* @var \DOMElement $address_items */
                $address_class = $address_items->getAttribute('class');
                if (strpos($address_class, 'xxx-table-branches-bank__item-name') !== FALSE) {
                  $department['name'] = trim($address_items->textContent);
                }
                if (strpos($address_class, 'xxx-table-branches-bank__item-address') !== FALSE) {
                  $department['address'] = trim($address_items->textContent);
                }
                if (strpos($address_class, 'xxx-info-block__icon--metro-red') !== FALSE) {
                  $department['metro'] = trim($address_items->textContent);
                }
              }
            }
            elseif (strpos($class, 'xxx-table-branches-bank__item-phone') !== FALSE) {
              $department['phone'] = trim($childNode->textContent);
            }
            elseif (strpos($class, 'xxx-table-branches-bank__item-time') !== FALSE) {
              $department['work_time'] = trim($childNode->textContent);
            }

          }

          if (!empty($department['name']) && !empty($department['address'])) {
            $new_nid = $this->createUpdateDepartment($city, $bank, $department);
            if ($new_nid) {
              $new_department_addresses[] = $department['address'];
            }
          }

        }
      }
    }

    return $new_department_addresses;
  }

  /**
   * Получение отделений банка
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseDepartments() {
    $new_departments = [];
    $cities = $this->getCities();
    foreach ($cities as $city) {
      $this->log($city['code'], 'parseDepartments');
      $city_tid = $this->getCityIdByCode($city['code']);
      $banks = $this->getBanksInCity($city_tid);
      foreach ($banks as $bank) {
        $new_department_addresses = $this->parseDepartmentsByCityByBank($city['code'], $bank['code']);
        if ($new_department_addresses) {
          $new_departments[$city['code']][$bank['code']][] = [
            'url' => self::site_scheme . $city['code'] . '.' . self::site_domain . '/bank/' . $bank['code'] . '/departments-tab',
            'addresses' => $new_department_addresses,
          ];
        }
      }
    }
    if ($new_departments) {
      $message = '';
      foreach ($new_departments as $city => $banks) {
        $message .= 'В городе ' . $city . ' появились новые отделения' . PHP_EOL;
        foreach ($banks as $bank_code => $data) {
          $message .= 'В банке ' . $bank_code . PHP_EOL;
          $message .= 'Адреса:' . PHP_EOL;
          foreach ($data as $datum) {
            $message .= implode(', ', $datum['addresses']);
          }
          $message .= PHP_EOL;
        }
      }
      $this->sendMailToAll('Новые отделения', $message);
    }
  }

  /**
   * Создание / обновление банкомата
   *
   * @param $city
   * @param $bank
   * @param $atm
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createUpdateATM($city, $bank, $atm) {
    $new_nid = FALSE;
    $city_id = $this->getCityIdByCode($city);
    $bank_id = $this->getBankIdByCode($bank);

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'atm');
    $query->condition('field_bank', $bank_id);
    $query->condition('field_city', $city_id);
    $query->condition('field_address', $atm['address']);
    $nids = $query->execute();

    // если такой банкомат есть - обновляем его
    if ($nids) {
      $node = Node::load(current($nids));
      $node->set('title', $atm['name']);
      $node->set('field_metro', $atm['metro']);
      $node->set('field_phone', $atm['phone']);
      $node->set('field_work_time', $atm['work_time']);
      $node->save();
    }
    else {
      Node::create([
        'type' => 'atm',
        'title' => $atm['name'],
        'field_city' => [
          'target_id' => $city_id,
        ],
        'field_bank' => [
          'target_id' => $bank_id,
        ],
        'field_address' => $atm['address'],
        'field_metro' => $atm['metro'],
        'field_phone' => $atm['phone'],
        'field_work_time' => $atm['work_time'],
      ])->save();
      $new_nid = TRUE;
    }

    return $new_nid;
  }

  /**
   * Получение банкоматов по городу и банку
   *
   * @param $city
   * @param $bank
   *
   * @return false|array
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function parseATMByCityByBank($city, $bank) {
    $new_atm_addresses = FALSE;
    // начинаем с первой страницы
    $current_page = 1;
    $have_next_page = TRUE;
    // если есть следующая страница
    while ($have_next_page) {
      $have_next_page = FALSE;
      $client = \Drupal::httpClient();

      $url = self::site_scheme . $city . '.' . self::site_domain . '/bank/' . $bank . '/bankomats-tab';
      // для первой страницы параметр page не указываем
      if ($current_page > 1) {
        $url .= '?page=' . $current_page;
      }

      $request = $client->get($url, self::guzzle_client_options);
      $status = $request->getStatusCode();
      if ($status != 200) {
        return FALSE;
      }

      $file_contents = $request->getBody()->getContents();
      $html = Html::load($file_contents);

      // получаем следующую страницу
      $xpath = new DomXPath($html);
      $classname = 'pagination';
      $ul = $xpath->query("//ul[contains(@class, '$classname')]");
      if ($ul->length) {
        foreach ($ul->item(0)->childNodes as $li) {
          if ($li->firstChild->tagName == 'a') {
            $page_num = $li->firstChild->getAttribute('data-page');
            if (($page_num + 1) > $current_page) {
              $current_page = $page_num + 1;
              $have_next_page = TRUE;
              break;
            }
          }
        }
      }

      // обходим таблицу с банкоматами
      $xpath = new DomXPath($html);
      $divs = $xpath->query("//table[contains(@class, 'xxx-table-list')]//tbody//div[@class='xxx-table-branches-bank__item-inner']");

      if ($divs->length) {
        foreach ($divs as $div) {
          $atm = [
            'name' => NULL,
            'address' => NULL,
            'metro' => NULL,
            'phone' => NULL,
            'work_time' => NULL,
          ];
          /* @var \DOMElement $div */
          foreach ($div->childNodes as $childNode) {
            /* @var \DOMElement $childNode */
            $class = $childNode->getAttribute('class');


            if ($class == 'xxx-table-branches-bank__item-name-address') {
              foreach ($childNode->childNodes as $address_items) {
                /* @var \DOMElement $address_items */
                $address_class = $address_items->getAttribute('class');
                if (strpos($address_class, 'xxx-table-branches-bank__item-name') !== FALSE) {
                  $atm['name'] = trim($address_items->textContent);
                }
                if (strpos($address_class, 'xxx-table-branches-bank__item-address') !== FALSE) {
                  $atm['address'] = trim($address_items->textContent);
                }
                if (strpos($address_class, 'xxx-info-block__icon--metro-red') !== FALSE) {
                  $atm['metro'] = trim($address_items->textContent);
                }
              }
            }
            elseif (strpos($class, 'xxx-table-branches-bank__item-phone') !== FALSE) {
              $atm['phone'] = trim($childNode->textContent);
            }
            elseif (strpos($class, 'xxx-table-branches-bank__item-time') !== FALSE) {
              $atm['work_time'] = trim($childNode->textContent);
            }
          }

          if (!empty($atm['name']) && !empty($atm['address'])) {
            $new_nid = $this->createUpdateATM($city, $bank, $atm);
            if ($new_nid) {
              $new_atm_addresses[] = $atm['address'];
            }
          }
        }
      }
    }

    return $new_atm_addresses;
  }

  /**
   * Получение банкоматов
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseATM() {
    $new_atms = [];
    $cities = $this->getCities();
    foreach ($cities as $city) {
      $this->log($city['code'], 'parseATM');
      $city_tid = $this->getCityIdByCode($city['code']);
      $banks = $this->getBanksInCity($city_tid);
      foreach ($banks as $bank) {
        $new_atms_addresses = $this->parseATMByCityByBank($city['code'], $bank['code']);
        if ($new_atms_addresses) {
          $new_atms[$city['code']][$bank['code']][] = [
            'url' => self::site_scheme . $city['code'] . '.' . self::site_domain . '/bank/' . $bank['code'] . '/bankomats-tab',
            'addresses' => $new_atms_addresses,
          ];
        }
      }
    }

    if ($new_atms) {
      $message = '';
      foreach ($new_atms as $city => $banks) {
        $message .= 'В городе ' . $city . ' появились новые банкоматы' . PHP_EOL;
        foreach ($banks as $bank_code => $data) {
          $message .= 'В банке ' . $bank_code . PHP_EOL;
          $message .= 'Адреса:' . PHP_EOL;
          foreach ($data as $datum) {
            $message .= implode(', ', $datum['addresses']);
          }
          $message .= PHP_EOL;
        }
      }
      $this->sendMailToAll('Новые банкоматы', $message);
    }
  }

  /**
   * Устанавливаем pool городов для обработки
   */
  public function setCitiesPool() {
    $pool = [];
    $cities = $this->getCities();
    foreach ($cities as $city) {
      $pool[$city['code']] = ['code' => $city['code'], 'name' => $city['name']];
    }
    \Drupal::state()->set('i_cities_pool', $pool);
  }

  /**
   * Устанавливаем pool городов для обработки у которых нет курсов
   */
  public function setPoolEmptyCities() {
    $pool = $this->getNoExchangeRatesCities();
    \Drupal::state()->set('i_cities_pool', $pool);
  }

  /**
   * Получение среза из pool городов для обработки
   *
   * @param int $count
   *
   * @return mixed
   */
  public function getSliceCitiesPool($count = 100) {
    $pool = \Drupal::state()->get('i_cities_pool', []);
    $slice = array_slice($pool, 0, $count, TRUE);
    foreach ($slice as $key => $item) {
      unset($pool[$key]);
    }
    \Drupal::state()->set('i_cities_pool', $pool);

    return $slice;
  }

  /**
   * Сохранение курса ЦБРФ
   *
   * @param $currency
   * @param $course
   * @param $date
   * @param $unit
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createCBRFCurrencyRate($currency, $course, $date, $unit) {
    $currency_id = $this->getCurrencyIdByCode($currency);

    if ($currency_id) {
      $query = \Drupal::entityQuery('node');
      $query->condition('type', self::course_cbrf_type);
      $query->condition('field_date', $date);
      $query->condition('field_currency', $currency_id);
      $nids = $query->execute();

      // если такой курс есть - обновляем его
      if (!$nids) {
        Node::create([
          'type' => self::course_cbrf_type,
          'title' => 'ЦБРФ ' . $currency . ' ' . $date . ' ',
          'field_currency' => [
            'target_id' => $currency_id,
          ],
          'field_date' => $date,
          'field_unit' => $unit,
          'field_courser_cbrf' => $course,
        ])->save();
      }
      else {
        $node = Node::load(current($nids));
        $node->set('field_unit', $unit);
        $node->set('field_courser_cbrf', $course);
        $node->save();
      }
    }
  }

  /**
   * Получение и сохранение курсов ЦБРФ
   *
   * @return false
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function parseCBRFExchangeRates() {
    $client = \Drupal::httpClient();

    $url = self::site_url . '/currency/cbrf';

    $request = $client->get($url, self::guzzle_client_options);
    $status = $request->getStatusCode();
    if ($status != 200) {
      return FALSE;
    }

    $file_contents = $request->getBody()->getContents();
    $html = Html::load($file_contents);

    // получаем следующую страницу
    $xpath = new DomXPath($html);
    $thead = $xpath->query("//table[contains(@class, 'standard')]//tr[1]");
    /* @var \DOMNodeList $thead */
    // если нет курса на завтра то не продолжаем
    if (trim($thead->item(0)->childNodes->item(2)->textContent) != 'Курс на завтра') {
      return FALSE;
    }

    $trs = $xpath->query("//tr[@class='currency']");

    foreach ($trs as $tr) {
      $today = new \DateTime();
      $is_friday = ($today->format('w') == 5);
      $today->modify('+1 day');

      /* @var \DOMElement $tr */
      $currency_code = explode('/', parse_url($tr->childNodes->item(0)->childNodes->item(0)
        ->getAttribute('href'))['path'])[3];
      $currency_name = trim($tr->childNodes->item(0)->childNodes->item(0)->childNodes->item(2)->textContent);
      $course_value = trim($tr->childNodes->item(2)->childNodes->item(0)->textContent);
      $unit = trim(str_replace($currency_code, '', strtolower($tr->childNodes->item(3)->childNodes->item(0)->textContent)));

      $this->createCurrency($currency_name, $currency_code);
      $this->createCBRFCurrencyRate($currency_code, $course_value, $today->format('d.m.Y'), $unit);
      // если сегодня пятница
      if ($is_friday) {
        // заполняем курсы еще на два дня вперед
        for ($i = 1; $i <= 2; $i++) {
          $today->modify('+1 day');
          $this->createCBRFCurrencyRate($currency_code, $course_value, $today->format('d.m.Y'), $unit);
        }
      }
    }
  }

  /**
   * Получить города в которых нет курсов
   *
   * @return array
   */
  protected function getNoExchangeRatesCities() {
    $cities = $this->getCities();

    $empty_cities = [];
    $now_date = new \DateTime();
    foreach ($cities as $city) {
      $count = 0;
      $city_id = $this->getCityIdByCode($city['code']);
      $banks = $this->getBanksInCity($city_id);
      foreach ($banks as $bank) {
        $bank_id = $this->getBankIdByCode($bank['code']);
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'course');
        $query->condition('field_city', $city_id);
        $query->condition('field_bank', $bank_id);
        $query->condition('field_date_updated', $now_date->format('d.m.Y'));
        $nid_count = $query->count()->execute();
        $count += $nid_count;
        if ($count) {
          break;
        }
      }

      if (!$count) {
        $empty_cities[$city['code']] = ['code' => $city['code'], 'name' => $city['name']];
      }
    }

    return $empty_cities;
  }

  /**
   * Проверка наличия курсов на текущий день
   * Если отсутствуют то шлем письма
   */
  public function checkNoExchangeRates() {
    $now_date = new \DateTime();

    $empty_cities = $this->getNoExchangeRatesCities();

    if ($empty_cities) {
      $empty_cities_names = [];
      foreach ($empty_cities as $empty_city) {
        $empty_cities_names[] = $empty_city['name'];
      }
      $this->sendMailToAll('Отсутствуют курсы', 'Нет курсов (' . $now_date->format('d.m.Y') . ') в городах (' . count($empty_cities_names) . '): ' . PHP_EOL . implode(', ', $empty_cities_names));
    }
  }

}

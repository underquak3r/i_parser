<?php

namespace Drupal\i_parser\Controller;

use Drupal\file\Entity\File;
use Drupal\i_parser\BanksParser;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use morphos\Russian\GeographicalNamesInflection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CurrencyApi {

  const city_vid = 'cities';
  const currency_vid = 'currency';

  const allowed_ip = [
    '1.2.3.4',
    '5.6.7.8',
  ];

  protected function log($data, $label = NULL, $with_time = TRUE) {
    $out = '';
    if ($with_time) {
      $out = date('H:i:s d-m-Y', time()) . "\n";
    }
    $out .= ($label ? $label . ': ' : '') . print_r($data, TRUE) . "\n";
    $file = '/var/www/i_parser/data/mod-tmp/bank_api_debug.txt';
    file_put_contents($file, $out, FILE_APPEND);
  }

  /**
   * Проверка ip
   *
   * @param $ip
   *
   * @return bool
   */
  protected function isAllowedIP($ip) {
    return in_array($ip, self::allowed_ip);
  }

  /**
   * Проверка доступа
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   */
  protected function checkAccess(Request $request) {
    if (!$this->isAllowedIP($request->getClientIp())) {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * Получение списка городов
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function getCities(Request $request) {
    $this->checkAccess($request);

    $cities = [];

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', self::city_vid);
    $tids = $query->execute();

    foreach ($tids as $tid) {
      $term = Term::load($tid);
      $banks = $term->get('field_bank_link');
      $banks_array = [];
      foreach ($banks->referencedEntities() as $bank) {
        $banks_array[] = [
          'code' => $bank->get('field_code')->value,
        ];
      }
      $cities[] = [
        'name' => $term->get('name')->value,
        'prepositional' => GeographicalNamesInflection::getCase($term->get('name')->value, 'предложный'),
        'genitive' => GeographicalNamesInflection::getCase($term->get('name')->value, 'родительный'),
        'code' => $term->get('field_code')->value,
        'banks' => $banks_array,
      ];
    }

    return new JsonResponse(['cities' => $cities]);

  }

  /**
   * Получение списка банков
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getBanks(Request $request) {
    $this->checkAccess($request);

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $banks = [];

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'bank');
    $nids = $query->execute();

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $phone = [];
      foreach ($node->get('field_phone')->getValue() as $item) {
        $phone[] = $item['value'];
      }

      $logo = NULL;
      if ($node->get('field_logo')->target_id) {
        $logoFile = File::load($node->get('field_logo')->target_id);
        $logo = $host . $logoFile->createFileUrl();
      }

      $icon = NULL;
      if ($node->get('field_icon')->target_id) {
        $iconFile = File::load($node->get('field_icon')->target_id);
        $icon = $host . $iconFile->createFileUrl();
      }

      $banks[] = [
        'code' => $node->get('field_code')->value,
        'name' => $node->get('title')->value,
        'full_name' => $node->get('field_full_name')->value,
        'bik' => $node->get('field_bik')->value,
        'head_office_address' => $node->get('field_head_office_address')->value,
        'net_assets' => $node->get('field_net_assets')->value,
        'date_of_registration' => $node->get('field_date_of_registration')->value,
        'deposits' => $node->get('field_deposits')->value,
        'credits' => $node->get('field_credits')->value,
        'registration_number' => $node->get('field_registration_number')->value,
        'site_link' => $node->get('field_bank_link')->value,
        'phone' => $phone,
        'authorized_capital' => $node->get('field_authorized_capital')->value,
        'logo' => $logo,
        'icon' => $icon,
      ];
    }

    return new JsonResponse(['banks' => $banks]);
  }

  public function getCurrencies(Request $request) {
    $this->checkAccess($request);

    $currencies = [];

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', self::currency_vid);
    $tids = $query->execute();

    foreach ($tids as $tid) {
      $term = Term::load($tid);
      $currencies[] = [
        'name' => $term->get('name')->value,
        'code' => $term->get('field_code')->value,
      ];
    }

    return new JsonResponse(['currencies' => $currencies]);

  }

  public function getExchangeRates(Request $request) {
    $this->checkAccess($request);

    $rates = [];
    $bank = $request->query->get('bank');
    $city = $request->query->get('city');
    $date = $request->query->get('date');

    if ($city) {
      $parser = new BanksParser();
      $city_id = $parser->getCityIdByCode($city);
      $query = \Drupal::entityQuery('node');
      $query->condition('type', 'course');
      $query->condition('field_city', $city_id);
      if ($bank) {
        $bank_id = $parser->getBankIdByCode($bank);
        $query->condition('field_bank', $bank_id);
      }
      if ($date) {
        $query->condition('field_date_updated', $date);
      }
      else {
        $now_date = new \DateTime();
        $query->condition('field_date_updated', $now_date->format('d.m.Y'));
      }
      $nids = $query->execute();

      foreach ($nids as $nid) {
        $course = Node::load($nid);
        $rates[$course->get('field_bank')->entity->get('field_code')->value][] = [
          'currency' => $course->get('field_currency')->entity->get('field_code')->value,
          'unit' => $course->get('field_unit')->value,
          'updated' => $course->get('field_updated')->value,
          'purchase' => $course->get('field_purchase')->value,
          'sale' => $course->get('field_sale')->value,
        ];
      }
    }
    return new JsonResponse(['rates' => $rates]);
  }

  /**
   * Получение отделений банка в городе
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getDepartments(Request $request) {
    $this->checkAccess($request);

    $departments = [];
    $bank = $request->query->get('bank');
    $city = $request->query->get('city');

    if ($city && $bank) {
      $parser = new BanksParser();
      $city_id = $parser->getCityIdByCode($city);
      $bank_id = $parser->getBankIdByCode($bank);

      $query = \Drupal::entityQuery('node');
      $query->condition('type', 'department');
      $query->condition('field_city', $city_id);
      $query->condition('field_bank', $bank_id);

      $nids = $query->execute();

      foreach ($nids as $nid) {
        $department = Node::load($nid);
        $departments[] = [
          'name' => $department->get('title')->value,
          'address' => $department->get('field_address')->value,
          'metro' => $department->get('field_metro')->value,
          'phone' => $department->get('field_phone')->value,
          'work_time' => $department->get('field_work_time')->value,
        ];
      }
    }
    return new JsonResponse(['departments' => $departments]);
  }

  /**
   * Получение банкоматов по банку и городу
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getATMs(Request $request) {
    $this->checkAccess($request);

    $atms = [];
    $bank = $request->query->get('bank');
    $city = $request->query->get('city');

    if ($city && $bank) {
      $parser = new BanksParser();
      $city_id = $parser->getCityIdByCode($city);
      $bank_id = $parser->getBankIdByCode($bank);

      $query = \Drupal::entityQuery('node');
      $query->condition('type', 'atm');
      $query->condition('field_city', $city_id);
      $query->condition('field_bank', $bank_id);

      $nids = $query->execute();

      foreach ($nids as $nid) {
        $atm = Node::load($nid);
        $atms[] = [
          'name' => $atm->get('title')->value,
          'address' => $atm->get('field_address')->value,
          'metro' => $atm->get('field_metro')->value,
          'phone' => $atm->get('field_phone')->value,
          'work_time' => $atm->get('field_work_time')->value,
        ];
      }
    }
    return new JsonResponse(['atms' => $atms]);
  }

  /**
   * Получение курсов ЦБРФ на указанную дату
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getCBRFExchangeRates(Request $request) {
    $this->checkAccess($request);

    $rates = [];
    $date = $request->query->get('date');

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'course_cbrf');
    if ($date) {
      $query->condition('field_date', $date);
    }
    else {
      $now_date = new \DateTime();
      $date = $now_date->format('d.m.Y');
      $query->condition('field_date', $date);
    }
    $nids = $query->execute();

    foreach ($nids as $nid) {
      $course = Node::load($nid);
      $rates[$course->get('field_currency')->entity->get('field_code')->value] = [
        'rate' => $course->get('field_courser_cbrf')->value,
        'unit' => $course->get('field_unit')->value,
      ];
    }
    return new JsonResponse(['rates' => $rates, 'date' => $date]);
  }

}
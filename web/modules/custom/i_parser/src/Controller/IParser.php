<?php

namespace Drupal\i_parser\Controller;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\i_parser\BanksParser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class IParser {

  const allowed_ip = [
    '1.2.3.4',
    '5.6.7.8',
  ];

  /**
   * Проверка ip
   *
   * @param $ip
   *
   * @return bool
   */
  protected function isAllowedIP($ip) {
    return in_array($ip, self::allowed_ip);
  }

  /**
   * Проверка доступа
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   */
  protected function checkAccess(Request $request) {
    if (!$this->isAllowedIP($request->getClientIp())) {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function setPool(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->setCitiesPool();
    return new Response('ok');
  }

  public function setPoolEmptyCities(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->setPoolEmptyCities();
    return new Response('ok');
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function doParseSlice(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->parseCurrency();
    return new Response('ok');
  }

  public function doParseDepartments(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->parseDepartments();
    return new Response('ok');
  }

  public function doParseATM(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->parseATM();
    return new Response('ok');
  }

  public function updateBanksInCities(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->updateBanksInCities();
    return new Response('ok');
  }

  public function updateBanksDetails(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->updateBanksDetails();
    return new Response('ok');
  }

  public function updateBanksImages(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->updateBanksImages();
    return new Response('ok');
  }

  public function doParseCBRFExchangeRates(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->parseCBRFExchangeRates();
    return new Response('ok');
  }

  public function doCheckNoExchangeRates(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $parser->checkNoExchangeRates();
    return new Response('ok');
  }

  public function exchangeRateList(Request $request) {
    $this->checkAccess($request);

    $parser = new BanksParser();
    $cities = $parser->getCities();

    $rows = [];

    $city_counter = 0;
    $now_date = new \DateTime();
    foreach ($cities as $city) {
      $city_counter++;
      $count = 0;
      $city_id = $parser->getCityIdByCode($city['code']);
      $banks = $parser->getBanksInCity($city_id);
      foreach ($banks as $bank) {
        $bank_id = $parser->getBankIdByCode($bank['code']);
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'course');
        $query->condition('field_city', $city_id);
        $query->condition('field_bank', $bank_id);
        $query->condition('field_date_updated', $now_date->format('d.m.Y'));
        $nid_count = $query->count()->execute();
        $count += $nid_count;
      }

      $rows[] = [
        'counter' => [
          'data' => $city_counter,
        ],
        'city' => [
          'data' => $city['code'],
        ],
        'price_change_today' => [
          'data' => $count,
        ],
      ];
    }

    return [
      '#title' => 'Exchange Rate List (' . $now_date->format('d.m.Y') . ')',
      '#type' => 'table',
      '#header' => ['#', 'City', 'count rates'],
      '#rows' => $rows,
    ];
  }

}